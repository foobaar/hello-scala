package example

object Hello extends Greeting with App {
  println(greeting)
  if(true) {
     println("hello")   
    }
  var test = 200    
  println("hello world")
}

trait Greeting {
  lazy val greeting: String = "hello"
}
